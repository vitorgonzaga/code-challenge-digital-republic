import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
import React from 'react';
import { Form } from 'react-bootstrap';

export default function Input({
  id,
  labelText,
  name,
  onChange,
  type,
  value,
}) {
  return (
    <Form.Label key={id} htmlFor={name} style={{ margin: 15 }}>
      { labelText }
      <Form.Control
        data-testid={id}
        id={id}
        key={id}
        name={name}
        onChange={onChange}
        type={type}
        value={value}
      />
    </Form.Label>
  );
}

Input.propTypes = {
  id: PropTypes.string,
  labelText: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  value: PropTypes.string,
}.isRequired;
