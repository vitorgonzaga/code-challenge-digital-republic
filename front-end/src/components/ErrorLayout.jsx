import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useContext } from 'react';
import { Card } from 'react-bootstrap';
import Context from '../context/Context';

export default function ErrorLayout() {
  const { errorLogs } = useContext(Context);

  const renderErrorList = (arrErrors) => (
    arrErrors.map((error, index) => (
      <Card.Text key={error[index]}>
        { error.message || error}
      </Card.Text>
    ))
  );

  return (
    <Card style={{ color: '#000' }}>
      <Card.Body>
        <Card.Title>Erros encontrados</Card.Title>
        { errorLogs.length > 0
          ? renderErrorList(errorLogs)
          : null }
      </Card.Body>
    </Card>
  );
}
