import Joi from 'joi';
import PropTypes from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import Context from './Context';

export default function Provider({ children }) {
  const [metreageWallsState, setMetreageWallsState] = useState({});
  const [response, setResponse] = useState({});
  // 18: 0,
  // 3.6: 0,
  // 2.5: 0,
  // 0.5: 0,
  const [errorLogs, setErrorLogs] = useState([]);
  const [liters, setLiters] = useState(0);
  const [data, setData] = useState([
    {
      id: 1,
      measurements: {
        height: 0, width: 0, window: 0, door: 0,
      },
    },
    {
      id: 2,
      measurements: {
        height: 0, width: 0, window: 0, door: 0,
      },
    },
    {
      id: 3,
      measurements: {
        height: 0, width: 0, window: 0, door: 0,
      },
    },
    {
      id: 4,
      measurements: {
        height: 0, width: 0, window: 0, door: 0,
      },
    },
  ]);

  const schemaWall = Joi.object({
    height: Joi.number().min(0).precision(1).required()
      .messages({
        'number.precision': 'valor inválido',
        'number.required': 'altura obrigatória',
      }),
    width: Joi.number().min(0).precision(1).required()
      .messages({
        'number.required': 'largura obrigatória',
      }),
    window: Joi.number().integer().messages({
      'number.integer': 'quantidade de janela(s) deve ser expressa em números inteiros',
    }),
    door: Joi.number().integer().messages({
      'number.integer': 'quantidade de portas deve ser expressa em números inteiros',
    }),
  }).with('height', 'width');

  // To persist the data from localStorage
  useEffect(() => {
    const localData = JSON.parse(localStorage.getItem('wallsData'));
    if (!localData) {
      localStorage.setItem('wallsData', JSON.stringify(data));
    } else {
      setData(localData);
    }
  }, []);

  useEffect(() => {
    const localError = JSON.parse(localStorage.getItem('errorLogsLocal'));
    if (!localError) {
      localStorage.setItem('errorLogsLocal', JSON.stringify([]));
    }
  }, []);

  const defaultLengthWindow = (2 * 1.2);
  const defaultLengthDoor = (0.8 * 1.9);

  const updateErrorLocal = (array) => {
    localStorage.setItem('errorLogsLocal', JSON.stringify([...array]));
  };

  // Source: https://javascript.plainenglish.io/how-to-add-to-an-array-in-react-state-3d08ddb2e1dc
  const addErrorMessage = async (objMessage) => {
    await setErrorLogs((oldArray) => [...oldArray, objMessage]);
  };

  const checkHeightWallRule = async (id, door, height, balanceMetreage) => {
    if ((door > 0) && (height < 1.90 || balanceMetreage < 0.30)) {
      try {
        const errorMessage = {
          message: `Parede ${id}: A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta`,
        };
        await addErrorMessage(errorMessage);
        updateErrorLocal(errorLogs);
        return true;
      } catch (error) {
        await addErrorMessage(error.message);
      }
    }
    return false;
  };

  const checkFiftyPercRule = async (id, winPlusDoorMetreage, halfWallMetreage) => {
    if (winPlusDoorMetreage > halfWallMetreage) {
      try {
        const errorMessage = {
          message: `Parede ${id}: O total de área das portas e janelas deve ser no máximo 50% da área de parede`,
        };
        await addErrorMessage(errorMessage);
        updateErrorLocal(errorLogs);
        return true;
      } catch (error) {
        await addErrorMessage(error.message);
      }
    }
    return false;
  };

  const checkMetreageMin = async (id, height, width, metreageWall) => {
    if ((height > 0 && width > 0) && (metreageWall < 1 || metreageWall > 15)) {
      try {
        const errorMessage = {
          message: `Parede ${id}: Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes.`,
        };
        await addErrorMessage(errorMessage);
        updateErrorLocal(errorLogs);
        return true;
      } catch (error) {
        await addErrorMessage(error.message);
      }
    }
    return false;
  };

  const checkMandatoryInputs = async (id, height, width, window, door) => {
    try {
      const { error } = schemaWall.validate({
        height, width, window, door,
      });

      if (error) {
        const errorMessage = { message: `Parede: ${id} - ${error.details[0].message}` };
        await addErrorMessage(errorMessage);
        updateErrorLocal(errorLogs);
        return true;
      }
    } catch (error) {
      await addErrorMessage(error.message);
    }

    return false;
  };

  const calculateConsumption = async () => {
    const arrCans = [[18, 0], [3.6, 0], [2.5, 0], [0.5, 0]];
    const arrTemp = [];
    let newValue = 0;
    let balance = liters;
    try {
      arrCans.forEach(async (valor) => { // Object.entries(response)
        if (balance > parseFloat(valor[0])) {
          balance -= parseFloat(valor[0]);
          newValue = valor[1] + 1;
          arrTemp.push(parseFloat(valor[0]));
        }
      });
      const xlCan = arrTemp[0];
      if (xlCan > 0) {
        await setResponse((prevState) => ({ ...prevState, [xlCan]: newValue }));
      }
    } catch (error) {
      await addErrorMessage(error.message);
    }
    return true;
  };

  const calculateLiters = async () => {
    try {
      const metreageTotal = await Object.values(metreageWallsState)
        .reduce((acc, actual) => acc + actual, 0);
      const doubleliters = (metreageTotal / 5);
      await setLiters(() => (doubleliters));
      await calculateConsumption();
    } catch (error) {
      await addErrorMessage(error.message);
      // console.log('calculateLiters -> error', error);
    }
  };

  const validateForm = async (infoWalls) => {
    await setErrorLogs([]);
    await setMetreageWallsState({});
    infoWalls.forEach(async (obj) => {
      let metreageWall = 0;
      let metreageWindows = 0;
      let metreageDoors = 0;

      const {
        id,
        measurements: {
          height, width, window, door,
        },
      } = obj;

      try {
        // ===========================================================
        // Checking types and mandatory informations
        // ===========================================================
        await checkMandatoryInputs(id, height, width, window, door);
        // ===========================================================
        // consolidating variables
        // ===========================================================
        metreageWindows = (window * defaultLengthWindow);
        metreageDoors = (door * defaultLengthDoor);
        metreageWall = ((height * width) - metreageWindows - metreageDoors);
        // console.log('infoWalls.forEach -> total', total);
        // total = { ...total, [id]: metreageWall };
        setMetreageWallsState((prevState) => ({ ...prevState, [id]: metreageWall }));
        const halfMetreageWall = (metreageWall / 2);
        const windowsPlusDoorsMetrage = (metreageWindows + metreageDoors);
        const balanceHeightWallMinusDoor = (height - 1.90);
        // ===========================================================
        // Checking business rules
        // ===========================================================
        await checkFiftyPercRule(id, windowsPlusDoorsMetrage, halfMetreageWall);
        await checkHeightWallRule(id, door, height, balanceHeightWallMinusDoor);
        await checkMetreageMin(id, height, width, metreageWall);
      } catch (error) {
        await addErrorMessage(error.message);
      }
    });
    await calculateLiters();
  };

  const context = useMemo(() => ({
    data,
    errorLogs,
    liters,
    setData,
    response,
    validateForm,
    metreageWallsState,
  }));

  return (
    <Context.Provider value={context}>
      {children}
    </Context.Provider>
  );
}

Provider.propTypes = {
  children: PropTypes.node,
}.isRequired;
