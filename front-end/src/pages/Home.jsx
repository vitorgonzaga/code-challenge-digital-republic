import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useContext } from 'react';
import { Container, Form } from 'react-bootstrap';
import ErrorLayout from '../components/ErrorLayout';
import Button from '../components/forms/Button';
import ResponseLayout from '../components/ResponseLayout';
import Wall from '../components/Wall';
import Context from '../context/Context';

export default function Home() {
  const { data, errorLogs, validateForm } = useContext(Context);

  const handleClick = async (wallsInfo) => {
    await validateForm(wallsInfo);
  };

  return (
    <div className="App-header">
      <Container>
        <header>
          <h1>Calculadora de Tinta</h1>
        </header>
      </Container>
      <Container>
        {
          errorLogs.length > 0
            ? <ErrorLayout />
            : null
        }
      </Container>
      <Container>
        <ResponseLayout />
      </Container>
      <Container>
        <main>
          <Form>
            {
              data && data.map((item, index) => (
                <Wall
                  key={`${item.id}`}
                  id={item.id}
                  measurements={item.measurements}
                  position={index}
                />
              ))
              }

            <Button
              id="home_page__button-submit"
              onClick={() => handleClick(data)}
              text="CALCULAR"
            />
          </Form>
        </main>
      </Container>
    </div>
  );
}
