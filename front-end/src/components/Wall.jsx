import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import Context from '../context/Context';
import Input from './forms/Input';

export default function Wall({ id, measurements, position }) {
  const { data, setData } = useContext(Context); // setData
  // const wallsInfo = JSON.parse(localStorage.getItem('wallsData'));

  const handleChange = ({ target: { name, value } }) => {
    const newArr = [...data];
    newArr[position].measurements[name] = value;
    localStorage.setItem('wallsData', JSON.stringify(newArr));
    setData(newArr);
  };

  const arrLabels = [
    'Altura (metros): ',
    'Largura (metros): ',
    'Qtde Janela(s): ',
    'Qtde Porta(s): ',
  ];

  return (
    <div>
      <h2>{`Parede ${id}`}</h2>
      {
        measurements && Object.keys(measurements).map((item, index) => (
          <Input
            id={`home_page__input-${item}-${id}`}
            key={`${item}-${id}`}
            labelText={arrLabels[index]}
            name={`${item}`}
            onChange={(element) => handleChange(element)}
            type="number"
            value={data[position].measurements[item]}
          />
        ))
      }
    </div>
  );
}

Wall.propTypes = {
  id: PropTypes.number.isRequired,
  measurements: PropTypes.shape(
    {
      heigth: PropTypes.number,
      width: PropTypes.number,
      window: PropTypes.number,
      door: PropTypes.number,
    },
  ).isRequired,
  position: PropTypes.number.isRequired,
};

// Wall.defaultProps = {
//   measurements: PropTypes.s
// }
