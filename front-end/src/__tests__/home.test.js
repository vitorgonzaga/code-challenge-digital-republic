import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { act } from 'react-dom/test-utils';
import Provider from '../context/Provider';
import Home from '../pages/Home';

export const arrWalls = [1, 2, 3, 4];

export const elements = ['height', 'width', 'window', 'door'];

export const selectors = {
  home_page: {
    input: {
      height: 'home_page__input-height-',
      width: 'home_page__input-width-',
      window: 'home_page__input-window-',
      door: 'home_page__input-door-',
    },
  },
};

describe('1 - Verificando se os componentes estão renderizados na tela', () => {

  it('Verificando se existe o header da página Home', () => {
    render(<Home />, { wrapper: Provider });
    screen.getAllByText('Calculadora de Tinta');
  });

  test('Verificando se os inputs das 4 paredes estão na tela', () => {
    render(<Home />, { wrapper: Provider });
    arrWalls.forEach((wall, index) => {
      screen.getByTestId(`home_page__input-${elements[index]}-${wall}`);
    });
  });

  test('Verificando se existe um botão de calcular', () => {
    render(<Home />, { wrapper: Provider });
    const btnCalc = screen.getByTestId('home_page__button-submit');
    expect(btnCalc).toBeInTheDocument();
    expect(btnCalc).toHaveProperty('type', 'button');
    expect(btnCalc).toHaveTextContent('CALCULAR');
  });

})

describe('2 - Verificando casos de uso que disparam erro', () => {
  let inputData = {};

  const errorMessages = {
    sizeWall: 'Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes',
    fiftyRule: 'O total de área das portas e janelas deve ser no máximo 50% da área de parede',
    heightWall: 'A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta'
  }

  test('referente ao tamanho mínimo e máximo das paredes', () => {

    inputData = {
      height: '0.5',
      width: '1',
      window: '0',
      door: '0'
    }

    render(<Home />, { wrapper: Provider });
    const inputHeigth = screen.getByTestId(`home_page__input-${elements[0]}-${1}`)
    const inputWidth = screen.getByTestId(`home_page__input-${elements[1]}-${1}`)
    // ==================================
    // Cleaning the input controls
    userEvent.clear(inputHeigth)
    userEvent.clear(inputWidth)
    // ==================================
    // Typing information to check the error response
    userEvent.type(inputHeigth, inputData.height)
    userEvent.type(inputWidth, inputData.width)
    // ==================================
    expect(inputHeigth).toHaveValue(parseFloat(inputData.height))
    expect(inputWidth).toHaveValue(parseFloat(inputData.width))
    const btnCalc = screen.getByTestId('home_page__button-submit');
    userEvent.click(btnCalc)
    // Due to assyncronous functions and complexity set timeout was needed
    setTimeout(() => {
      screen.getByText(errorMessages.sizeWall)
    }, 5000);
  })

  test('referente a regra que limita em 50% a participação de portas e janelas na parede', () => {

    inputData = {
      height: '2.5',
      width: '2',
      window: '1',
      door: '1',
    }
    act(() => {
      render(<Home />, { wrapper: Provider });
    })
    const inputHeigth = screen.getByTestId(`home_page__input-${elements[0]}-${1}`)
    const inputWidth = screen.getByTestId(`home_page__input-${elements[1]}-${1}`)
    const inputWindow = screen.getByTestId(`home_page__input-${elements[2]}-${1}`)
    const inputDoor = screen.getByTestId(`home_page__input-${elements[3]}-${1}`)

    // ==================================
    // Cleaning the input controls
    userEvent.clear(inputHeigth)
    userEvent.clear(inputWidth)
    userEvent.clear(inputWindow)
    userEvent.clear(inputDoor)

    act(() => {
      // ==================================
      // Typing information to check the error response
      userEvent.type(inputHeigth, inputData.height)
      userEvent.type(inputWidth, inputData.width)
      userEvent.type(inputWindow, inputData.window)
      userEvent.type(inputDoor, inputData.door)
      console.log('act -> inputDoor', inputDoor)

    })
    // ==================================
    expect(inputHeigth).toHaveValue(parseFloat(inputData.height))
    expect(inputWidth).toHaveValue(parseFloat(inputData.width))
    expect(inputWindow).toHaveValue(parseFloat(inputData.window))
    expect(inputDoor).toHaveValue(parseFloat(inputData.door))

    const btnCalc = screen.getByTestId('home_page__button-submit');

    act(() => {
      userEvent.click(btnCalc)
    })
    // Due to assyncronous functions and complexity set timeout was needed
    setTimeout(() => {
      screen.getByText(errorMessages.fiftyRule)
    }, 5000);

  })

  test('referente a altura da parede, que deve ser 30 centímetros maior que altura da porta', () => {

    inputData = {
      height: '2',
      width: '3',
      window: '0',
      door: '1'
    }
    act(() => {
      render(<Home />, { wrapper: Provider });
    })
    const inputHeigth = screen.getByTestId(`home_page__input-${elements[0]}-${1}`)
    const inputWidth = screen.getByTestId(`home_page__input-${elements[1]}-${1}`)
    const inputWindow = screen.getByTestId(`home_page__input-${elements[2]}-${1}`)
    const inputDoor = screen.getByTestId(`home_page__input-${elements[3]}-${1}`)

    // ==================================
    // Cleaning the input controls
    userEvent.clear(inputHeigth)
    userEvent.clear(inputWidth)
    userEvent.clear(inputWindow)
    userEvent.clear(inputDoor)

    act(() => {
      // ==================================
      // Typing information to check the error response
      userEvent.type(inputHeigth, inputData.height)
      userEvent.type(inputWidth, inputData.width)
      userEvent.type(inputWindow, inputData.window)
      userEvent.type(inputDoor, inputData.door)

    })
    // ==================================
    expect(inputHeigth).toHaveValue(parseFloat(inputData.height))
    expect(inputWidth).toHaveValue(parseFloat(inputData.width))
    expect(inputWindow).toHaveValue(parseFloat(inputData.window))
    expect(inputDoor).toHaveValue(parseFloat(inputData.door))

    const btnCalc = screen.getByTestId('home_page__button-submit');

    act(() => {
      userEvent.click(btnCalc)
    })
    // Due to assyncronous functions and complexity set timeout was needed
    setTimeout(() => {
      screen.getByText(errorMessages.heightWall)
    }, 5000);

  })

})

