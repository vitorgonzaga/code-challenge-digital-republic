# [Code Challenge - Digital Republic](https://gitlab.com/digitalrepublic/code-challenge)

Desafio Técnico associado a processo seletivo visando desenvolver uma calculadora de Tinta.

Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L

## Regras de negócio

- Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes
- O total de área das portas e janelas deve ser no máximo 50% da área de parede
- A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
- Cada janela possui as medidas: 2,00 x 1,20 mtos
- Cada porta possui as medidas: 0,80 x 1,90
- Cada litro de tinta é capaz de pintar 5 metros quadrados
- Não considerar teto nem piso.
- As variações de tamanho das latas de tinta são:

    - 0,5 L
    - 2,5 L
    - 3,6 L
    - 18 L



## Instalação

Através do terminal realize o clone numa pasta local: ```git clone <url do repositório>```;

Execute ```npm install``` na pasta raiz "DIGITAL_REPUBLIC_CODE_CHALLENGE";

Acesse a pasta front-end: ```cd front-end``` e execute ```npm install```

Em seguida, ainda na pasta front-end, digite no terminal ```npm start```

A estrutura de pastas ficou organizada dessa maneira, devido a intenção de desenvolver o back-end posteriomente, sendo assim ambas as pastas (front e back) ficariam dentro da pasta raiz.

## Executando os testes

Para visualizar os testes, execute o seguinte comando dentro da pasta "front-end"

```bash
  npm run test
```
Para visualizar o relatório de cobertura dos testes:

```bash
  npm test -- --coverage
```

## Screenshots

</br>

![App Screenshot](images/coverage_tests.png)

</br>

![App Screenshot](images/home_screenshot.png)

## 🔗 Links

[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://github.com/vitorgonzaga)

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/vitorgonzaga/)
