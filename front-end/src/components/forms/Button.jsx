import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
import React from 'react';
import { Button } from 'react-bootstrap';

function Btn({
  disabled,
  id,
  onClick,
  text,
}) {
  return (
    <Button
      variant="primary"
      type="button"
      onClick={onClick}
      data-testid={id}
      disabled={disabled}
    >
      { text }
    </Button>
  );
}

Btn.propTypes = {
  id: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

Btn.defaultProps = {
  disabled: false,
};

export default Btn;
