import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useContext } from 'react';
import { Card, Container } from 'react-bootstrap';
import Context from '../context/Context';

export default function ResponseLayout() {
  const { response } = useContext(Context);
  // console.log('ResponseLayout -> response', response);

  const renderResponse = () => (
    <Card style={{ color: '#000' }}>
      <Card.Body>
        <Card.Title>Lista de consumo por latas:</Card.Title>
        {
          Object.entries(response).map((can) => (
            <Card.Text key={`${can[0]}`}>{ `${can[1]} lata(s) de ${can[0]} litro(s)` }</Card.Text>
          ))
        }
      </Card.Body>
    </Card>
  );

  return (
    <Container>
      {
        Object.keys(response).length > 0
          ? renderResponse()
          : null
      }
    </Container>
  );
}
